const mongoose = require("mongoose");

const toolsSchema = new mongoose.Schema({
  title: {
    type: String,
    require: true
  },
  link: {
    type: String,
    require: true
  },
  description: {
    type: String,
    require: true
  },
  tags: [String]
});

const Tools = mongoose.model("Tools", toolsSchema);

export default Tools;