import User from '../models/User';
class UsersCrontroller {

  async create (req, res){
    try {
      const { name, email, password} = req.body;
      const users = await User.create({
        name,
        email,
        password
      });
  
      return res.status(201).send({ users });
    } catch (err) {
      console.log(err);
      return res.status(400).send({ error: "Registration failed." });
    }
  }
  
}

export default new UsersCrontroller;