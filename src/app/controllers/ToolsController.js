import Tools from '../models/Tools';

class ToolsCrontroller {

  async create (req, res){
    try {
      const { title, link, description, tags } = req.body;
      const tools = await Tools.create({
        title,
        link,
        description,
        tags
      });
  
      return res.status(201).send({ tools });
    } catch (err) {
      return res.status(400).send({ error: "Registration failed." });
    }
  };
  
  async list (req, res){
    try {
      const tools = await Tools.find();
      
      return res.status(200).send({ tools });
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log('error',err);
      return res.status(400).send({ error: "Failed." });
    }

  };
  
  async listbytag (req, res){
    const tag = req.query.tag;
    try {
      const {_id} = await Tools.where("tags", tag).select('node');
      const tools2 = await Tools.where("id", _id).select();
      
      return res.status(200).send({ tools2 });
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(err);
      return res.status(404).send({ error: "Failed." });
    }
  };

  async deleteone (req, res){
    const {id} = req.params;
      
      const result = await Tools.deleteOne({ _id:id });
      console.log('resultadoo',result.deletedCount);
      if(result.deletedCount <1){
        return res.status(404).send({ error: "Failed." });
      }
      else if(result.deletedCount >0){
        return res.status(200).send({ message: 'Sucess.' });
      }
  }; 

  async updateone (req, res){

    const { atribute, currentvalue, newvalue} = req.body;

    const result = await Tools.updateOne({ [atribute] : `${currentvalue}` }, { [atribute]: `${newvalue}` });


      if(result.n >0 && result.nModified >0){
        return res.status(200).send({ message: 'Sucess.' });
      }
      else {
        return res.status(404).send({ error: "Failed." });
      }
  };

}
export default new ToolsCrontroller;
