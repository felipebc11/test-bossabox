import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import User from '../models/User';

import authConfig from '../../config/auth.js';

class SessionController {
  async store(req, res){
    const { email, password } = req.body;
    const user = await User.where("email",email).select();
    const passwordhu = await User.where("email",email).select("passwordhash");

    if(user[0]== undefined){
      return res.status(401).json({error : 'User not found.'});
    }

    async function checkPassword (password){
      return bcrypt.compare(password, passwordhu[0].passwordhash);
    }

    if(! await checkPassword(password)){
      return res.status(401).json({error : 'Password does not match.'});
    }

    const { _id, name } = user[0];

    return res.json({
      user: {
        _id,
        name,
        email
      },
      token: jwt.sign({ _id }, authConfig.secret, {
        expiresIn: authConfig.expiresIn,
      }),
    });

  }
}

export default new SessionController;