import { Router } from 'express';

import UsersController from './app/controllers/UserController';
import SessionController from './app/controllers/SessionController';
import ToolsController from './app/controllers/ToolsController';

import authMiddleware from './app/middlewares/auth';

const routes = new Router();


routes.post('/login', SessionController.store);
routes.post('/users', UsersController.create);

routes.use(authMiddleware);

routes.post('/tools', ToolsController.create);
routes.get('/tools', ToolsController.list);
routes.get('/tools', ToolsController.listbytag);
routes.delete('/tools/:id', ToolsController.deleteone);
routes.put('/tools', ToolsController.updateone);


export default routes;