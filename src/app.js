import express from 'express';
import routes from './routes';
import DatabaseConfig from './config/database';

class App {
  constructor() {
    this.server = express();
    this.middlewares();
    this.routes();
    this.database();
  }

  middlewares() {
    this.server.use(express.json());
  }

  routes() {
    this.server.use(routes);
  }
  database() {
    DatabaseConfig.makeConnection();
  }
}

export default new App().server;
