const mongoose = require("mongoose");

const path = require("path");


require("dotenv").config({
  path: path.resolve(__dirname, "./.env")
});

class DatabaseConfig {
  async makeConnection(){
    mongoose.Promise = global.Promise;
    try{
      await mongoose.connect(process.env.MONGO_CREDENTIALS, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      });
    } catch(err){
        console.log('error', err);
    }
  }
}
export default new DatabaseConfig;

