# Test Bossabox

Bossabox back-end test.

## Instalação

Para começar, execute o comando abaixo, assim instalando as depedências necessárias no seu ambiente de execução.

```sh
$ npm install
```

```

## Principais comandos

```sh
$ npm run dev
$ npm run build
$ npm run start
```

## Documentação Postman:
https://documenter.getpostman.com/view/8267484/SzmcZJQm

## Pré-requisitos

Configure os arquivos presentes na pasta src/config para poder executar a aplicação.


## Licença

MIT © [Felipe Bastos](https://github.com/felipebc11)
